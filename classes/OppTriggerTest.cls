@istest

public Class OppTriggerTest{
    public static testMethod void validateOppTrigger(){    
        Test.startTest();
        Account testAccount = new Account(Name = 'Test Account');
        insert testAccount;
    
        Opportunity opp = new Opportunity(Name = 'New Opp',
                                         AccountId = testAccount.Id,
                                         StageName = 'Prospecting',
                                         CloseDate = System.today()
                                         );
        insert opp;                          
        Test.stopTest();
    }
}